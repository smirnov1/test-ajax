<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 02.11.2018
 * Time: 16:30
 * Обработчик формы регистрации.
 */

$logFile = 'event.log';

$arUsers = [
    [
        'id' => 1,
        'firstname' => 'Ivan',
        'lastname' => 'Smirnov',
        'email' => 'smirnov@greensight.ru',
    ],
    [
        'id' => 2,
        'firstname' => 'Ivan',
        'lastname' => 'Ivanov',
        'email' => 'ivanov@mail.ru',
    ],
    [
        'id' => 3,
        'firstname' => 'Fedor',
        'lastname' => 'Fedorov',
        'email' => 'fedorov@ya.ru',
    ],
];

if (empty($_POST['email'])) {
    file_put_contents($logFile, '[' . date('d.m.Y H:i:s') . '] - прямой вызов обработчика' . PHP_EOL, FILE_APPEND);
    die(json_encode([
        'ok' => false
    ]));
}

foreach ($arUsers as $arUser) {
    if ($_POST['email'] == $arUser['email']) {
        file_put_contents($logFile, '[' . date('d.m.Y H:i:s') . '] - пользователь с email ' . $_POST['email'] . ' существует' . PHP_EOL, FILE_APPEND);
        die(json_encode([
            'ok' => false
        ]));
    }
}

file_put_contents($logFile, '[' . date('d.m.Y H:i:s') . '] - пользователь с email ' . $_POST['email'] . ' НЕ существует' . PHP_EOL, FILE_APPEND);
die(json_encode([
    'ok' => true
]));